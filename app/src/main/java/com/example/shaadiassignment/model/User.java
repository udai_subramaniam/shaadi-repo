package com.example.shaadiassignment.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "user_table")
public class User {

    @Embedded
    @SerializedName("name")
    private Name name;

    @Embedded
    @SerializedName("picture")
    private Picture picture;

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "email")
    @SerializedName("email")
    private String email;

    @ColumnInfo(name="status")
    private int status;

    public User(Name name, Picture picture, String email, int status) {
        this.name = name;
        this.picture = picture;
        this.email = email;
        this.status = status;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public static class Name {

        @ColumnInfo(name="first_name")
        @SerializedName("first")
        private String userFirstName;

        @ColumnInfo(name="last_name")
        @SerializedName("last")
        private String userLastName;

        public Name(String userFirstName, String userLastName) {
            this.userFirstName = userFirstName;
            this.userLastName = userLastName;
        }

        public String getUserFirstName() {
            return userFirstName;
        }

        public void setUserFirstName(String userFirstName) {
            this.userFirstName = userFirstName;
        }

        public String getUserLastName() {
            return userLastName;
        }

        public void setUserLastName(String userLastName) {
            this.userLastName = userLastName;
        }

    }

    public static class Picture {

        @ColumnInfo(name="image_url")
        @SerializedName("large")
        private String userImageURL;

        public Picture(String userImageURL) {
            this.userImageURL = userImageURL;
        }

        public String getUserImageURL() {
            return userImageURL;
        }

        public void setUserImageURL(String userImageURL) {
            this.userImageURL = userImageURL;
        }

    }

}