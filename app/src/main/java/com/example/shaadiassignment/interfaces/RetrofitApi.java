package com.example.shaadiassignment.interfaces;

import com.example.shaadiassignment.model.Response;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface RetrofitApi {

    @GET("/api/?results=10")
    Single<Response> getData();
}
