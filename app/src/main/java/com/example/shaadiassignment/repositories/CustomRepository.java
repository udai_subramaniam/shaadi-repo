package com.example.shaadiassignment.repositories;

import android.content.Context;

import com.example.shaadiassignment.db.UserDatabase;
import com.example.shaadiassignment.services.RetrofitApiService;
import com.example.shaadiassignment.interfaces.UserDao;
import com.example.shaadiassignment.model.Response;
import com.example.shaadiassignment.model.User;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CustomRepository {

    private RetrofitApiService retrofitApiService = new RetrofitApiService();
    private UserDao userDao;

    public CustomRepository(Context context) {
        UserDatabase userDatabase = UserDatabase.getDatabase(context);
        userDao = userDatabase.userDao();
    }

    public Single<Response> fetchDataFromAPI() {
        return retrofitApiService.getRetrofitInstance().getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<User>> fetchDataFromDB() {
        return userDao.getAllUsers();
    }

    public void insertAllIntoDB(List<User> users) {
        UserDatabase.databaseWriteExecutor.execute(() -> userDao.insertAll(users));
    }

    public void updateUser(User user) {
        UserDatabase.databaseWriteExecutor.execute(() -> userDao.update(user));
    }

}