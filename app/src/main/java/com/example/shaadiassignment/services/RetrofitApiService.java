package com.example.shaadiassignment.services;

import com.example.shaadiassignment.interfaces.RetrofitApi;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitApiService {

    private static final String BASE_URL = "https://randomuser.me";

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    private static RetrofitApi retrofitApi = retrofit.create(RetrofitApi.class);

    public RetrofitApi getRetrofitInstance() {
        return retrofitApi;
    }
}
