package com.example.shaadiassignment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shaadiassignment.adapters.CustomRecyclerViewAdapter;
import com.example.shaadiassignment.model.User;
import com.example.shaadiassignment.viewmodels.CustomViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.rv_userList)
    RecyclerView rv_userList;

    @BindView(R.id.pb_loader)
    ProgressBar pb_loader;

    Context context;
    CustomViewModel customViewModel;
    CustomRecyclerViewAdapter customRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;

        customViewModel = ViewModelProviders.of(this).get(CustomViewModel.class);

        //fetch users from db and API
        customViewModel.fetchUsers();

        //observe list changes
        customViewModel.getUserList().observe(this, users -> customRecyclerViewAdapter.updateList(users));

        //observe progress bar changes
        customViewModel.getIsLoading().observe(this, isLoading ->
                pb_loader.setVisibility(isLoading ? View.VISIBLE : View.GONE));

        //observe if error occurs
        customViewModel.getIsError().observe(this, isError -> {
            if(isError) {
                Toast.makeText(context, getResources().getString(R.string.failedToConnect), Toast.LENGTH_SHORT).show();
            }
        });

        customRecyclerViewAdapter = new CustomRecyclerViewAdapter(this, new ArrayList<>());
        rv_userList.setLayoutManager(new LinearLayoutManager(this));
        rv_userList.setAdapter(customRecyclerViewAdapter);
    }

    public void updateUser(User user) {
       customViewModel.updateUser(user);
    }
}
