package com.example.shaadiassignment.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.shaadiassignment.model.Response;
import com.example.shaadiassignment.model.User;
import com.example.shaadiassignment.repositories.CustomRepository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CustomViewModel extends AndroidViewModel {

    private MutableLiveData<List<User>> userList = new MutableLiveData<>();

    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();

    private MutableLiveData<Boolean> isError = new MutableLiveData<>();

    private List<User> tempUserList = new ArrayList<>();

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private CustomRepository customRepository;

    public CustomViewModel(@NonNull Application application) {
        super(application);
        customRepository = new CustomRepository(application);
    }

    public void fetchUsers() {

        //fetch from db
        compositeDisposable.add(
            customRepository.fetchDataFromDB()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<List<User>>() {
                        @Override
                        public void onSuccess(List<User> users) {
                            //notify list observer only if items present
                            if(users.size() > 0) {
                                tempUserList.addAll(users);
                                userList.setValue(users);
                                isLoading.setValue(false);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            //not setting isError here, as user could still get data from API, will be set if that too fails
                        }
                    })
        );

        //fetch from api
        compositeDisposable.add(
            customRepository.fetchDataFromAPI()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<Response>() {
                        @Override
                        public void onSuccess(Response response) {
                            tempUserList.addAll(response.getUsersList());
                            userList.setValue(tempUserList);
                            customRepository.insertAllIntoDB(response.getUsersList());
                            isLoading.setValue(false);
                            isError.setValue(false);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            isLoading.setValue(false);
                            isError.setValue(true);
                        }
                    })
        );
    }

    public void updateUser(User user) {
        customRepository.updateUser(user);
    }

    public LiveData<List<User>> getUserList() {
        return userList;
    }

    public LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public LiveData<Boolean> getIsError() {
        return isError;
    }

    @Override
    public void onCleared() {
        compositeDisposable.clear();
    }
}
