package com.example.shaadiassignment.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.shaadiassignment.MainActivity;
import com.example.shaadiassignment.R;
import com.example.shaadiassignment.model.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomRecyclerViewAdapter extends RecyclerView.Adapter<CustomRecyclerViewAdapter.CustomViewHolder> {

    private Context context;
    private List<User> userList;
    private static final int USER_ACCEPTED = 1;
    private static final int USER_REJECTED = -1;

    public CustomRecyclerViewAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_item_row, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        User currentUser = userList.get(position);

        holder.tv_user_name.setText(context.getResources().getString(R.string.userFullName, currentUser.getName().getUserFirstName(),
                currentUser.getName().getUserLastName()));
        Glide.with(holder.itemView)
                .load(currentUser.getPicture().getUserImageURL())
                .into(holder.iv_user_photo);

        //set status if user already accepted/rejected
        if(currentUser.getStatus() == 1) {
            holder.b_accept.setText(context.getResources().getString(R.string.accepted));
            holder.b_accept.setVisibility(View.VISIBLE);
            holder.b_reject.setVisibility(View.GONE);
        } else if(currentUser.getStatus() == -1) {
            holder.b_reject.setText(context.getResources().getString(R.string.rejected));
            holder.b_reject.setVisibility(View.VISIBLE);
            holder.b_accept.setVisibility(View.GONE);
        } else {
            holder.b_accept.setText(context.getResources().getString(R.string.accept));
            holder.b_reject.setText(context.getResources().getString(R.string.reject));
            holder.b_accept.setVisibility(View.VISIBLE);
            holder.b_reject.setVisibility(View.VISIBLE);
        }

        holder.b_accept.setOnClickListener(v -> {
            //take action only if status not accepted to prevent unnecessary DB write
            if(currentUser.getStatus() != USER_ACCEPTED) {
                holder.b_accept.setText(context.getResources().getString(R.string.accepted));
                holder.b_reject.setVisibility(View.GONE);

                currentUser.setStatus(USER_ACCEPTED);
                //update status in DB asynchronously
                ((MainActivity) context).updateUser(currentUser);
            }
        });

        holder.b_reject.setOnClickListener(v -> {
            //take action only if status not rejected to prevent unnecessary DB write
            if(currentUser.getStatus() != USER_REJECTED) {
                holder.b_reject.setText(context.getResources().getString(R.string.rejected));
                holder.b_accept.setVisibility(View.GONE);

                currentUser.setStatus(USER_REJECTED);
                ((MainActivity) context).updateUser(currentUser);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_user_photo)
        ImageView iv_user_photo;
        @BindView(R.id.tv_user_name)
        TextView tv_user_name;
        @BindView(R.id.b_reject)
        Button b_reject;
        @BindView(R.id.b_accept)
        Button b_accept;

        CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateList(List<User> userList) {
        this.userList = userList;
        notifyDataSetChanged();
    }
}
