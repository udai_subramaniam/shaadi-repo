# README #

This README documents necessary information regarding the Shaadi.com assignment.

### Points to note ###

- Basic application that fetches new users on every launch, appends to end of existing list(DB list) in UI.
- Stores list in DB on fetching data and whenever user accepts/rejects a match.
- Made use of simple UI and generic error messages to focus on architecture.
- Added comments throughout code for better understanding.

### Architecture and Libraries ###

- MVVM, RxJava, Room, Retrofit, Glide, Butterknife